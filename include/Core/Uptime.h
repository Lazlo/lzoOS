#ifndef D_Uptime_H
#define D_Uptime_H

/**********************************************************
 *
 * Uptime is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void Uptime_Create(void);
void Uptime_Destroy(void);

uint8_t Uptime_GetSeconds(void);
uint8_t Uptime_GetMinutes(void);
uint8_t Uptime_GetHours(void);
void Uptime_SetSeconds(const uint8_t s);
void Uptime_SetMinutes(const uint8_t m);
void Uptime_SetHours(const uint8_t h);
void Uptime_Tick(void);
void Uptime_ToStr(char *s);

#endif  /* D_FakeUptime_H */
