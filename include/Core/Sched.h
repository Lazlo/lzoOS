#ifndef D_Sched_H
#define D_Sched_H

/**********************************************************
 *
 * Sched is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

typedef uint16_t delay_t;

void Sched_Create(void);
void Sched_Destroy(void);

uint8_t Sched_AddTask(void (*fp)(void), const delay_t periode, const delay_t delay);
void Sched_Update(void);
void Sched_Dispatch(void);

uint8_t SchedSpy_TasksInstalled(void);
uint8_t SchedSpy_TaskExecutionDue(const uint8_t pid);

#endif  /* D_FakeSched_H */
