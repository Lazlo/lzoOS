#ifndef D_String_H
#define D_String_H

/**********************************************************
 *
 * String is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void String_Create(void);
void String_Destroy(void);

char *strrev(char *s);

#endif  /* D_FakeString_H */
