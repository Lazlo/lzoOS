#ifndef D_Stdio_H
#define D_Stdio_H

/**********************************************************
 *
 * Stdio is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void Stdio_Create(void);
void Stdio_Destroy(void);

char *itoa(const uint16_t val, char *s, const uint8_t radix);

#endif  /* D_FakeStdio_H */
