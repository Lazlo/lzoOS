#ifndef D_lzoOSBuildTime_H
#define D_lzoOSBuildTime_H

///////////////////////////////////////////////////////////////////////////////
//
//  lzoOSBuildTime is responsible for recording and reporting when
//  this project library was built
//
///////////////////////////////////////////////////////////////////////////////

class lzoOSBuildTime
  {
  public:
    explicit lzoOSBuildTime();
    virtual ~lzoOSBuildTime();
    
    const char* GetDateTime();

  private:
      
    const char* dateTime;

    lzoOSBuildTime(const lzoOSBuildTime&);
    lzoOSBuildTime& operator=(const lzoOSBuildTime&);

  };

#endif  // D_lzoOSBuildTime_H
