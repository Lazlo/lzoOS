#include "lzoOSBuildTime.h"

lzoOSBuildTime::lzoOSBuildTime()
: dateTime(__DATE__ " " __TIME__)
{
}

lzoOSBuildTime::~lzoOSBuildTime()
{
}

const char* lzoOSBuildTime::GetDateTime()
{
    return dateTime;
}

