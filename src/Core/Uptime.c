#include "Uptime.h"

typedef struct UptimeStruct
{
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hours;
} UptimeStruct;

static UptimeStruct uptime;

void Uptime_Create(void)
{
	uptime.seconds = 0;
	uptime.minutes = 0;
	uptime.hours = 0;
}

void Uptime_Destroy(void)
{
}

uint8_t Uptime_GetSeconds(void)
{
	return uptime.seconds;
}

uint8_t Uptime_GetMinutes(void)
{
	return uptime.minutes;
}

uint8_t Uptime_GetHours(void)
{
	return uptime.hours;
}

void Uptime_SetSeconds(const uint8_t s)
{
	uptime.seconds = s;
}

void Uptime_SetMinutes(const uint8_t m)
{
	uptime.minutes = m;
}

void Uptime_SetHours(const uint8_t h)
{
	uptime.hours = h;
}

void Uptime_Tick(void)
{
	if (++uptime.seconds == 60)
	{
		uptime.seconds = 0;
		uptime.minutes++;
	}
	if (uptime.minutes == 60)
	{
		uptime.minutes = 0;
		uptime.hours++;
	}
	if (uptime.hours == 24)
	{
		uptime.hours = 0;
	}
}

void Uptime_ToStr(char *s)
{
	const char sep = ':';
	uint8_t i = 0;
	s[i++] = (char)('0' + (uptime.hours / 10));
	s[i++] = (char)('0' + (uptime.hours % 10));
	s[i++] = sep;
	s[i++] = (char)('0' + (uptime.minutes / 10));
	s[i++] = (char)('0' + (uptime.minutes % 10));
	s[i++] = sep;
	s[i++] = (char)('0' + (uptime.seconds / 10));
	s[i++] = (char)('0' + (uptime.seconds % 10));
	s[i] = '\0';
}
