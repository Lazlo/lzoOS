#include "Sched.h"

#define SCHED_MAX_TASKS 4

typedef struct TaskStruct {
	void (*fp)(void);
	delay_t periode;
	delay_t delay;
	uint8_t run;
} TaskStruct;

static TaskStruct task[SCHED_MAX_TASKS];

static void installTask(TaskStruct *t, void (*fp)(void), const delay_t periode, const delay_t delay)
{
	t->fp = fp;
	t->periode = periode;
	t->delay = delay;
	t->run = 0;
}

static void deleteTask(TaskStruct *t)
{
	installTask(t, 0, 0, 0);
}

static uint8_t validTask(TaskStruct *t)
{
	return t->fp ? 1 : 0;
}

static void updateTask(TaskStruct *t)
{
	if (!validTask(t))
		return;
	if (t->delay > 0)
		t->delay--;
	if (t->delay != 0)
		return;
	if (t->periode)
		t->delay = t->periode;
	t->run = 1;
}

static void dispatchTask(TaskStruct *t)
{
	if (!validTask(t))
		return;
	if (t->run == 0)
		return;
	t->run--;
	(*(t->fp))();
	if (t->delay)
		return;
	deleteTask(t);
}

static void foreachTask(void (*fp)(TaskStruct *t))
{
	uint8_t i;
	for (i = 0; i < SCHED_MAX_TASKS; i++)
		(fp)(&task[i]);
}

void Sched_Create(void)
{
	foreachTask(deleteTask);
}

void Sched_Destroy(void)
{
}

uint8_t Sched_AddTask(void (*fp)(void), const delay_t periode, const delay_t delay)
{
	uint8_t i;
	TaskStruct *t;
	for (i = 0; i < SCHED_MAX_TASKS; i++) {
		t = &task[i];
		if (!validTask(t))
			break;
	}
	if (i == SCHED_MAX_TASKS)
		return 0;
	installTask(t, fp, periode, delay);
	return (uint8_t)(i + 1);
}

void Sched_Update(void)
{
	foreachTask(updateTask);
}

void Sched_Dispatch(void)
{
	foreachTask(dispatchTask);
}

/* TODO Move to mock */
uint8_t SchedSpy_TasksInstalled(void)
{
	uint8_t i;
	uint8_t cnt = 0;
	for (i = 0; i < SCHED_MAX_TASKS; i++)
		if (validTask(&task[i]))
			cnt++;
	return cnt;
}

/* TODO Move to spy */
uint8_t SchedSpy_TaskExecutionDue(const uint8_t pid)
{
	return task[(pid - 1)].run;
}
