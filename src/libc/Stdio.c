#include "Stdio.h"
#include "String.h"

void Stdio_Create(void)
{
}

void Stdio_Destroy(void)
{
}

char *itoa(const uint16_t val, char *s, const uint8_t radix)
{
	const char c[] = "0123456789ABCDEF";
	uint16_t tmp = val;
	uint8_t i = 0;
	do {
		s[i++] = c[(tmp % radix)];
		tmp = (uint16_t)(tmp / radix);
	} while (tmp);
	s[i] = '\0';
	return strrev(s);
}
