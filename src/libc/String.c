#include "String.h"
#include "string.h"

void String_Create(void)
{
}

void String_Destroy(void)
{
}

char *strrev(char *s)
{
	size_t len, i, z;
	char tmp;
	len = strlen(s);
	for (i = 0; i < (len / 2); i++)
	{
		z = len - 1 - i;
		tmp = s[z];
		s[z] = s[i];
		s[i] = tmp;
	}
	return s;
}
