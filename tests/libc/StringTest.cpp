extern "C"
{
#include "String.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(String)
{
    void setup()
    {
      String_Create();
    }

    void teardown()
    {
       String_Destroy();
    }
};

TEST(String, strrev)
{
    char s[10] = "ollah";
    STRCMP_EQUAL("hallo", strrev(s));
}
