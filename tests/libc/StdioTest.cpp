extern "C"
{
#include "Stdio.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(Stdio)
{
    void setup()
    {
      Stdio_Create();
    }

    void teardown()
    {
       Stdio_Destroy();
    }
};

TEST(Stdio, itoa_retunsPtrToStrBufPassed)
{
    char s[10];
    POINTERS_EQUAL(&s, itoa(0, s, 10));
}

TEST(Stdio, itoa_returnZero)
{
    char s[10];
    STRCMP_EQUAL("0", itoa(0, s, 10));
}

TEST(Stdio, itoa_singleDigit)
{
    char s[10];
    STRCMP_EQUAL("9", itoa(9, s, 10));
}

TEST(Stdio, itoa_doubleDigit)
{
    char s[10];
    STRCMP_EQUAL("21", itoa(21, s, 10));
}

TEST(Stdio, itoa_maxVal)
{
	char s[10];
	STRCMP_EQUAL("65535", itoa(65535, s, 10));
}

TEST(Stdio, itoa_singleDigit_hex)
{
	char s[10];
	STRCMP_EQUAL("A", itoa(10, s, 16));
}

TEST(Stdio, itoa_doubleDigit_hex)
{
	char s[10];
	STRCMP_EQUAL("FF", itoa(255, s, 16));
}
