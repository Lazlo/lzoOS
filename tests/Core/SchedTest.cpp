extern "C"
{
#include "Sched.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(Sched)
{
    uint8_t max_tasks;

    void setup()
    {
      max_tasks = 4;
      Sched_Create();
    }

    void teardown()
    {
       Sched_Destroy();
    }
};

static uint8_t testTaskExecuted;

static void testTaskStub(void)
{
	testTaskExecuted++;
}

/* TODO Use TaskListSpy in this test */
TEST(Sched, Create_clearsTaskList)
{
	CHECK_EQUAL(0, SchedSpy_TasksInstalled());
}

TEST(Sched, AddTask_taskScheduledWithDelayButNotExecuted)
{
	testTaskExecuted = 0;
	Sched_AddTask(testTaskStub, 0, 10);
	Sched_Update();
	Sched_Dispatch();
	CHECK_EQUAL(0, testTaskExecuted);
}

TEST(Sched, AddTask_taskScheduledWithDelayAndExecuted)
{
	uint8_t i;
	testTaskExecuted = 0;
	Sched_AddTask(testTaskStub, 0, 10);
	for (i = 0; i < 10; i++)
		Sched_Update();
	Sched_Dispatch();
	CHECK_EQUAL(1, testTaskExecuted);
}

TEST(Sched, AddTask_taskScheduledWithoutDelayAndExecuted)
{
	testTaskExecuted = 0;
	Sched_AddTask(testTaskStub, 0, 0);
	Sched_Update();
	Sched_Dispatch();
	CHECK_EQUAL(1, testTaskExecuted);
}

TEST(Sched, AddTask_taskScheduledWithPeriodeAndExecutedRepeatedly)
{
	uint8_t i;
	testTaskExecuted = 0;
	Sched_AddTask(testTaskStub, 5, 0);
	for (i = 0; i < 50; i++) {
		Sched_Update();
		Sched_Dispatch();
	}
	CHECK_EQUAL(10, testTaskExecuted);
}

TEST(Sched, AddTask_returnPidOnSuccess)
{
	CHECK_EQUAL(1, Sched_AddTask(testTaskStub, 0, 0));
}

TEST(Sched, AddTask_returnZeroOnFailure)
{
	uint8_t i;
	for (i = 0; i < max_tasks; i++)
		Sched_AddTask(testTaskStub, 0, 0);
	CHECK_EQUAL(0, Sched_AddTask(testTaskStub, 0, 0));
}

/* TODO Test Sched_AddTask() will not add task with FP == 0 */

/* TODO Implement fake/spy function that will allow us to set a tasks FP
 * to 0, as this will no longer be allowed after making Sched_AddTask()
 * refuse to add empty tasks. */

TEST(Sched, Update_ignoreEmptyTask)
{
	uint8_t pid;
	uint8_t i;
	testTaskExecuted = 0;
	pid = Sched_AddTask(0, 0, 0);
	for (i = 0; i < 30; i++)
		Sched_Update();
	Sched_Dispatch();
	CHECK_EQUAL(0, SchedSpy_TaskExecutionDue(pid));
}

TEST(Sched, Dispatch_ignoreEmptyTask)
{
	testTaskExecuted = 0;
	/* this will potentially cause a segfault when dispatch will
	 * dereference the function pointer, which is 0. */
	Sched_AddTask(0, 0, 0);
	Sched_Update();
	Sched_Dispatch();
	CHECK_EQUAL(0, testTaskExecuted);
}

TEST(Sched, Dispatch_deleteTaskWithoutPeriodeAfterExecution)
{
	Sched_AddTask(testTaskStub, 0, 0);
	Sched_Update();
	Sched_Dispatch();
	CHECK_EQUAL(1, testTaskExecuted);
	CHECK_EQUAL(0, SchedSpy_TasksInstalled());
}

TEST(Sched, ListHandling_XXX_FindBetterName)
{
	uint8_t i;
	for (i = 0; i < max_tasks; i++)
		Sched_AddTask(testTaskStub, 0, 0);
	CHECK_EQUAL(max_tasks, SchedSpy_TasksInstalled());
}
