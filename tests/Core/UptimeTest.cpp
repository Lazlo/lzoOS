extern "C"
{
#include "Uptime.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(Uptime)
{
    void setup()
    {
      Uptime_Create();
    }

    void teardown()
    {
       Uptime_Destroy();
    }
};

TEST(Uptime, Create_SetSecondsToZero)
{
	CHECK_EQUAL(0, Uptime_GetSeconds());
}

TEST(Uptime, Create_SetMinutesToZero)
{
	CHECK_EQUAL(0, Uptime_GetMinutes());
}

TEST(Uptime, Create_SetHoursToZero)
{
	CHECK_EQUAL(0, Uptime_GetHours());
}

TEST(Uptime, SetSeconds)
{
	Uptime_SetSeconds(3);
	CHECK_EQUAL(3, Uptime_GetSeconds());
}

TEST(Uptime, SetMinutes)
{
	Uptime_SetMinutes(5);
	CHECK_EQUAL(5, Uptime_GetMinutes());
}

TEST(Uptime, SetHours)
{
	Uptime_SetHours(7);
	CHECK_EQUAL(7, Uptime_GetHours());
}

TEST(Uptime, Tick_IncrementsSeconds)
{
	Uptime_Tick();
	CHECK_EQUAL(1, Uptime_GetSeconds());
}

TEST(Uptime, Tick_IncrementsMinutesAndResetSecondsWhenSecondsReach60)
{
	Uptime_SetSeconds(59);
	Uptime_Tick();
	CHECK_EQUAL(0, Uptime_GetSeconds());
	CHECK_EQUAL(1, Uptime_GetMinutes());
}

TEST(Uptime, Tick_IncrementsHoursAndResetMinutesWhenMinutesReach60)
{
	Uptime_SetSeconds(59);
	Uptime_SetMinutes(59);
	Uptime_Tick();
	CHECK_EQUAL(0, Uptime_GetMinutes());
	CHECK_EQUAL(1, Uptime_GetHours());
}

TEST(Uptime, Tick_IncrementsHoursAndResetEverythingWhenHoursReach24)
{
	Uptime_SetSeconds(59);
	Uptime_SetMinutes(59);
	Uptime_SetHours(23);
	Uptime_Tick();
	CHECK_EQUAL(0, Uptime_GetSeconds());
	CHECK_EQUAL(0, Uptime_GetMinutes());
	CHECK_EQUAL(0, Uptime_GetHours());
}

TEST(Uptime, ToStr)
{
	char str[9];
	Uptime_SetSeconds(42);
	Uptime_SetMinutes(23);
	Uptime_SetHours(11);
	Uptime_ToStr(str);
	STRCMP_EQUAL("11:23:42", str);
}
